# README #


### What is this repository for? ###

* TreesectionBundle is a bundle to add a tree of categories to a project.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\TreesectionBundle\TreesectionBundle()


##In the config.yml you can add your own service##
treesection:
    permsservice: app.treesections
    interface: AppBundle:Treesection
