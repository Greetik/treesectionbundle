<?php

namespace Greetik\TreesectionBundle\Twig;

use Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType;

class AppExtension extends \Twig_Extension {

    private $sections;

    public function __construct($_sections) {
        $this->sections = $_sections;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('getTreesectionTitle', array($this, 'getTreesectionTitle'))
        );
    }


    /*Devuelve el título de una sección */
    public function getTreesectionTitle($id){
        return $this->sections->getTreesectionTitle($id);
    }
    
    
    public function getName() {
        return 'treesection_extension';
    }

}
