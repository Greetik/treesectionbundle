<?php

namespace Greetik\TreesectionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Treesection
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Greetik\TreesectionBundle\Repository\TreesectionRepository")
 * @ORM\Table(name="treesection", indexes={
 *      @ORM\Index(name="title", columns={"title"}),  @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="user", columns={"user"}),  @ORM\Index(name="lft", columns={"lft"}),  @ORM\Index(name="rgt", columns={"rgt"}),  @ORM\Index(name="root", columns={"root"}),  @ORM\Index(name="level", columns={"lvl"}),  @ORM\Index(name="isprivate", columns={"isprivate"}),  @ORM\Index(name="hidemenu", columns={"hidemenu"}),  @ORM\Index(name="protected", columns={"protected"}),  @ORM\Index(name="extra", columns={"extra"})
 * })
 */
class Treesection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string", length=255, nullable=true)
     */
    private $metatitle;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metadescription", type="string", length=255, nullable=true)
     */
    private $metadescription;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime")
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastedit", type="datetime", nullable=true)
     */
    private $lastedit;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

      /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;
    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Treesection", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;
    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $level;
    /**
     * @ORM\OneToMany(targetEntity="Treesection", mappedBy="parent")
     */
    private $children;
    
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="isprivate", type="boolean", nullable=true)
     */
    private $isprivate;
            
    /**
     * @var boolean
     * 
     * @ORM\Column(name="hidemenu", type="boolean", nullable=true)
     */
    private $hidemenu;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="protected", type="boolean", nullable=true)
     */
    private $protected;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="externallink", type="string", length=255, nullable=true)
     */
    private $externallink;
    
    
    /**
     * @var string
     * 
     * @ORM\Column(name="extra", type="string", length=255, nullable=true)
     */
    private $extra;
    
    
    private $images;
    
    public function __construct()
    {
         $this->children = new ArrayCollection();
    }
    
    
    
    public function getIndentedName() {
        if ($this->parent)
            return $this->parent->getIndentedName()." > " . $this->title;
        else return $this->title;
    }    

    public function getIndentedNameNoparent() {
        if ($this->parent)
            return str_repeat("  >  ", $this->level) . $this->title;
        else return $this->title;
    }    

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Treesection
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Treesection
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     *
     * @return Treesection
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     *
     * @return Treesection
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Treesection
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set lastedit
     *
     * @param \DateTime $lastedit
     *
     * @return Treesection
     */
    public function setLastedit($lastedit)
    {
        $this->lastedit = $lastedit;

        return $this;
    }

    /**
     * Get lastedit
     *
     * @return \DateTime
     */
    public function getLastedit()
    {
        return $this->lastedit;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Treesection
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Treesection
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Treesection
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Treesection
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Treesection
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return Treesection
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Treesection
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set isprivate
     *
     * @param boolean $isprivate
     *
     * @return Treesection
     */
    public function setIsprivate($isprivate)
    {
        $this->isprivate = $isprivate;

        return $this;
    }

    /**
     * Get isprivate
     *
     * @return boolean
     */
    public function getIsprivate()
    {
        return $this->isprivate;
    }

    /**
     * Set hidemenu
     *
     * @param boolean $hidemenu
     *
     * @return Treesection
     */
    public function setHidemenu($hidemenu)
    {
        $this->hidemenu = $hidemenu;

        return $this;
    }

    /**
     * Get hidemenu
     *
     * @return boolean
     */
    public function getHidemenu()
    {
        return $this->hidemenu;
    }

    /**
     * Set protected
     *
     * @param boolean $protected
     *
     * @return Treesection
     */
    public function setProtected($protected)
    {
        $this->protected = $protected;

        return $this;
    }

    /**
     * Get protected
     *
     * @return boolean
     */
    public function getProtected()
    {
        return $this->protected;
    }

    /**
     * Set externallink
     *
     * @param string $externallink
     *
     * @return Treesection
     */
    public function setExternallink($externallink)
    {
        $this->externallink = $externallink;

        return $this;
    }

    /**
     * Get externallink
     *
     * @return string
     */
    public function getExternallink()
    {
        return $this->externallink;
    }

    /**
     * Set extra
     *
     * @param string $extra
     *
     * @return Treesection
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set parent
     *
     * @param \Greetik\TreesectionBundle\Entity\Treesection $parent
     *
     * @return Treesection
     */
    public function setParent(\Greetik\TreesectionBundle\Entity\Treesection $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Greetik\TreesectionBundle\Entity\Treesection
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Greetik\TreesectionBundle\Entity\Treesection $child
     *
     * @return Treesection
     */
    public function addChild(\Greetik\TreesectionBundle\Entity\Treesection $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Greetik\TreesectionBundle\Entity\Treesection $child
     */
    public function removeChild(\Greetik\TreesectionBundle\Entity\Treesection $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
