<?php

namespace Greetik\TreesectionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Treesection
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Greetik\TreesectionBundle\Repository\TreesectionRepository")
 * @ORM\Table(name="treesection", indexes={
 *      @ORM\Index(name="title", columns={"title"}),  @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="user", columns={"user"}),  @ORM\Index(name="lft", columns={"lft"}),  @ORM\Index(name="rgt", columns={"rgt"}),  @ORM\Index(name="root", columns={"root"}),  @ORM\Index(name="level", columns={"lvl"}),  @ORM\Index(name="isprivate", columns={"isprivate"}),  @ORM\Index(name="hidemenu", columns={"hidemenu"}),  @ORM\Index(name="protected", columns={"protected"}),  @ORM\Index(name="extra", columns={"extra"})
 * })
 */
class Treesection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string", length=255, nullable=true)
     */
    private $metatitle;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metadescription", type="string", length=255, nullable=true)
     */
    private $metadescription;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime")
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastedit", type="datetime", nullable=true)
     */
    private $lastedit;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

      /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;
    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Treesection", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;
    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $level;
    /**
     * @ORM\OneToMany(targetEntity="Treesection", mappedBy="parent")
     */
    private $children;
    
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="isprivate", type="boolean", nullable=true)
     */
    private $isprivate;
            
    /**
     * @var boolean
     * 
     * @ORM\Column(name="hidemenu", type="boolean", nullable=true)
     */
    private $hidemenu;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="protected", type="boolean", nullable=true)
     */
    private $protected;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="externallink", type="string", length=255, nullable=true)
     */
    private $externallink;
    
    
    /**
     * @var string
     * 
     * @ORM\Column(name="extra", type="string", length=255, nullable=true)
     */
    private $extra;
    
    
    private $images;
    
    public function __construct()
    {
         $this->children = new ArrayCollection();
    }
    
    
    
    public function getIndentedName() {
        if ($this->parent)
            return $this->parent->getIndentedName()." > " . $this->title;
        else return $this->title;
    }    

    public function getIndentedNameNoparent() {
        if ($this->parent)
            return str_repeat("  >  ", $this->level) . $this->title;
        else return $this->title;
    }    

    
}
