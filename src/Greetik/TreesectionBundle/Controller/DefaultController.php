<?php

namespace Greetik\TreesectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\TreesectionBundle\Entity\Treesection;
use Greetik\TreesectionBundle\Form\Type\TreesectionType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * Render all the treesections of the project
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {

        return $this->render($this->getParameter('treesections.interface').':index.html.twig', array(
                    'insertAllow' => $this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('insert'),
                    'data' => $this->get($this->getParameter('treesections.permsservice'))->getTreesectionsByProject($idproject),
                    'idproject' => $idproject
        ));
    }

    /**
     * View an individual treesection, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id) {
        $treesectionObject = $this->get('treesection.tools')->getTreesectionObject($id);
        $treesection = $this->get($this->getParameter('treesections.permsservice'))->getTreesection($id);
        $editForm = $this->createForm( TreesectionType::class, $treesectionObject, array('_project'=>$treesectionObject->getProject(), '_uniqueroot'=>true));
        return $this->render($this->getParameter('treesections.interface').':view.html.twig', array(
            'item' => $treesection,
            'modifyAllow' => $this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('modify', $treesectionObject),
            'new_form' => $editForm->createView()
        ));
    }

    /**
     * Get the data of a new Treesection by Treesection and persis it
     * 
     * @param Treesection $item is received by Treesection Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $treesection = new Treesection();
        $newForm = $this->createForm( TreesectionType::class, $treesection, array('_project'=>$idproject, '_uniqueroot'=>true));
        
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get($this->getParameter('treesections.permsservice'))->insertTreesection($treesection, $idproject, $this->getUser()->getId());
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render($this->getParameter('treesections.interface').':insert.html.twig', array('idproject' => $idproject,'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('treesection_listtreesections', array('idproject' => $idproject)));
            }
        }

        return $this
                        ->render($this->getParameter('treesections.interface').':insert.html.twig', array(
                            'idproject' => $idproject, 
                            'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an treesection
     * 
     * @param int $id is received by Get Request
     * @param Treesection $item is received by Treesection Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $treesection = $this->get('treesection.tools')->getTreesectionObject($id);

        $editForm = $this->createForm( TreesectionType::class, $treesection, array('_project'=>$treesection->getProject(), '_uniqueroot'=>true/*, '_notparent'=>true*/));

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('treesections.permsservice'))->modifyTreesection($treesection);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }else{
                $this->addFlash('error', (string) $editForm->getErrors(true, true));
            }
        }
        
        
        return $this->redirect($this->generateUrl('treesection_viewtreesection', array('id' => $id)));
    }

    public function ordertreeAction(Request $request, $idproject) {
        $treesection = $request->get('sections');

        if ($request->getMethod() != "POST")
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No tiene permiso para hacer esta operación')), 200, array('Content-Type' => 'application/json'));
        try {
            $this->get($this->getParameter('treesections.permsservice'))->moveTreesections(json_decode($treesection, 1), $idproject, true);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage().' . '.$e->getLine().' . '.$e->getFile())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Delete a section from the data of a treesection
     * 
     * @param int $id is received by Get Request
     * @param Treesection $item is received by Treesection Request
     */
    public function deleteAction($id) {

        $treesection = $this->get('treesection.tools')->getTreesectionObject($id);
        $idproject = $treesection->getProject();
        $error=null;
       
        try{
            $this->get($this->getParameter('treesections.permsservice'))->deleteTreesection($treesection);
        }catch( \Exception $e){
            $error=$e->getMessage();
            $this->get('session')->getFlashBag()->set('error',$error);
            return $this->redirect($this->generateUrl('treesection_viewtreesection', array('id' => $id)));
        }
        return $this->redirect($this->generateUrl('treesection_listtreesections', array('idproject' => $idproject)));
    }

}
