<?php

namespace Greetik\TreesectionBundle\Services;

use Greetik\TreesectionBundle\Entity\Treesection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Treesectiontools {

    private $em;
    
    public function __construct($_entityManager) {
        $this->em = $_entityManager;
    }

    public function getTreesectionPerm($perm, $treesection=''){
        return true;
    }
    public function getUniqueRoot($idproject, $simple=false) {
       if ($simple) return $this->em->getRepository('TreesectionBundle:Treesection')->getUniqueRoot($idproject);
        
        
        //if we want a tree with a unique root we use this method
        $rootnodes = $this->em->getRepository('TreesectionBundle:Treesection')->getRootNodes();
        foreach ($rootnodes as $node)
            if ($node->getProject() == $idproject)
                return $node;
        
        return '';
    }

    public function moveTreesections($newtreesections, $idproject, $node = '') {
        $root = $this->getUniqueRoot($idproject);
        $move = $this->searchTreesectionsForMove($newtreesections, $root->getId(), $root);

        if ($move == 'parent')
            $this->searchTreesectionsForMove($newtreesections, $root->getId(), $root);

        $this->em->flush();
        //$originaltreesections = $this->getTreesectionsByProject($idproject);
    }

    protected function searchTreesectionsForMove($newtreesections, $oldparent, $root) {
        $i = 0;
        foreach ($newtreesections as $k => $v) {
            //get the original section
            $originaltreesection = $this->getTreesectionObject($v['id']);

            //compare the parents
            if ($originaltreesection->getParent()->getId() != $oldparent) {
                $originaltreesection->setParent($this->getTreesectionObject($oldparent));
                $this->em->persist($originaltreesection);
                return 'parent';
            }

            //
            //compare the position
            $originalposition = $this->getTreesectionPosition($originaltreesection);
            if ($originalposition != $i) {
                if ($originalposition > $i) {
                    $this->em->getRepository('TreesectionBundle:Treesection')->moveUp($originaltreesection, $originalposition - $i);
                } else {
                    $this->em->getRepository('TreesectionBundle:Treesection')->moveDown($originaltreesection, $i - $originalposition);
                }

                $this->em->persist($originaltreesection);
                return 'position';
            }

            if (isset($v['children'])) {
                $move = $this->searchTreesectionsForMove($v['children'], $v['id'], $root);
                if ($move == 'parent')
                    $this->searchTreesectionsForMove($v['children'], $v['id'], $root);
            }

            $i++;
        }


        return 0;
    }

    protected function getTreesectionPosition($treesection) {
        if (is_numeric($treesection))
            $treesection = $this->getTreesectionObject($treesection);

        $treesections = $this->getTreesectionsByProject($treesection->getProject(), $treesection->getParent());

        $i = 0;
        foreach ($treesections as $k => $v) {
            if ($v['id'] == $treesection->getId())
                return $i;
            $i++;
        }

        return $i;
    }


    protected function removeAllBranch($treesection) {
        foreach ($treesection as $v) {
            $section = $this->getTreesectionObject($v['id']);
            if (isset($v['__children']) && count($v['__children']) > 0)
                $this->removeAllBranch($v['__children']);

            $this->em->getRepository('TreesectionBundle:Treesection')->removeFromTree($section);
        }
    }

    public function getSimpleTreesectionsByProject($project, $node = '') {
        $repo = $this->em->getRepository('TreesectionBundle:Treesection');
        $query = $this->em
                ->createQueryBuilder()
                ->select('node.id, node.title, node.level, node.project, node.hidemenu')
                ->from('TreesectionBundle:Treesection', 'node')
                ->orderBy('node.root, node.lft', 'ASC')
                ->where((empty($node)?'node.root = 1':'node.id='.(is_array($node)? $node['id']:$node->getId())).' AND node.project='.$project)
                ->getQuery()
        ;
        
        $result = $query->getArrayResult();
        $completetreesection = $repo->buildTree($result);
        return $completetreesection;        
    }

    public function getTreesectionsByProject($project, $node = '') {
        if(empty( $node)) $node=$this->getUniqueRoot($project);
        if (is_numeric($node)) $node = $this->getTreesectionObject ($node);
        
        try{
            $completetreesection = $this->em->getRepository('TreesectionBundle:Treesection')->childrenHierarchy($node);
        }catch(\Exception $e){
            if ($e->getMessage()=='Node is not related to this repository'){
                $treesection = new Treesection();
                $treesection->setTitle('Raíz');
                $this->insertTreesection($treesection, $project, 0);
            }
            $node=$this->getUniqueRoot($project);
            $completetreesection = $this->em->getRepository('TreesectionBundle:Treesection')->childrenHierarchy($node);
        }
        
        foreach ($completetreesection as $keytree => $treesection) {
            if ($treesection['project'] != $project)
                unset($completetreesection[$keytree]);
            //else $completetreesection[$keytree]['name'] = $this->getTreesection ($treesection['id'])->getIndentedName();
        }
        
        return $completetreesection;
    }


    public function getTreesectionObject($id) {
        $treesection = $this->em->getRepository('TreesectionBundle:Treesection')->findOneById($id);

        if (!$treesection)
            throw new NotFoundHttpException('No se encuentra la sección');

        return $treesection;
    }

    public function getTreesection($id) {
        $treesection = $this->em->getRepository('TreesectionBundle:Treesection')->getTreesection($id);

        if (!$treesection)
            throw new NotFoundHttpException('No se encuentra la sección');

        return $treesection;
    }

    public function modifyTreesection($treesection) {
        $treesection->setLastedit(new \DateTime());
        
        $this->em->flush();
    }

    public function insertTreesection($treesection, $project, $user, $parent = '') {
        $treesection->setCreatedat(new \DateTime());
        $treesection->setProject($project);
        $treesection->setUser($user);

        if (!$parent)
            $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChild($treesection);
        else {
            if (is_numeric($parent)) {
                $parent = $this->getTreesectionObject($parent);
            }
            $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChildOf($treesection, $parent);
        }

        //$this->em->getClassMetaData(get_class($treesection))->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $this->em->flush();
    }


    public function deleteTreesection($treesection) {

        $this->removeAllBranch($treesection);

        $this->em->remove($treesection);
        $this->em->flush();
    }

    public function getPath($treesection) {
        if (is_numeric($treesection))
            $treesection = $this->getTreesectionObject ($treesection);
        if (is_array($treesection))
            $treesection = $this->getTreesectionObject ($treesection['id']);
        
        $path = $this->em->getRepository('TreesectionBundle:Treesection')->getPath($treesection);
        //delete the root
        unset($path[0]);
        return $path;
    }

    public function getAllChildrenOfBranch($node){
        $tree = $this->getTreesectionsByProject($this->getTreesection($node)['project'], $node);
        $children = array();
        $this->getAllChildrenOfTree($tree, $children);
        return $children;
    }
    
    protected function getAllChildrenOfTree($tree, &$children){
        foreach($tree as $k=>$v){
            $children[] = is_array($v) ? $v['id'] : $v->getId();
            if (isset($v['children']) && count($v['children']>0)) $this->getAllChildrenOfTree ($v['children'], $children);
        }   
    }


    public function getTreesectionTitle($id){
        return $this->em->getRepository('TreesectionBundle:Treesection')->getTreesectionTitle($id);
    }
    
    public function getTreesectionsForSelect($idproject) {
        return $this->em->getRepository('TreesectionBundle:Treesection')->getChildrenQueryBuilderProject($idproject)->getQuery()->getResult();
    }


}
