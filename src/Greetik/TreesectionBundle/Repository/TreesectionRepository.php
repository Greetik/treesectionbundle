<?php

namespace Greetik\TreesectionBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreesectionRepository
 *
 * @author Paco
 */
class TreesectionRepository extends NestedTreeRepository {

    private $commondatatoreturn = "s.id, IDENTITY(s.parent) as parent, s.title, s.tags, s.metatitle, s.metadescription, s.createdat, s.lastedit, s.project, s.user, s.isprivate, s.hidemenu, s.protected, s.externallink, s.extra";

    
    public function getTreesection($id){
        try{
        return $this->getEntityManager()
                        ->createQuery('SELECT '.$this->commondatatoreturn.', s.body FROM TreesectionBundle:Treesection s WHERE s.id=:id')
                        ->setParameter('id', $id)
                        ->getSingleResult();
        }catch(\Exception $e){
            
        }
        return '';
    }
    
    public function getTreesectionTitle($id){
        try{
            $section = $this->getEntityManager()
                            ->createQuery('SELECT s.title FROM TreesectionBundle:Treesection s WHERE s.id=:id')
                            ->setParameter('id', $id)
                            ->getSingleResult();
        }catch(\Exception $e){
            return '';
        }
        return $section['title'];
    }
    
    public function getUniqueRoot($idproject){
        
            $query = $this->getEntityManager()
                    ->createQueryBuilder()
                    ->select($this->commondatatoreturn)
                    ->from('TreesectionBundle:Treesection', 's')
                    ->where('node.parent is NULL AND node.project='.$idproject)
                    ->getQuery()
            ;
            
            $result = $query->getArrayResult();
            //if (count($result)>1) return '';
            return $result[0];        
    }
    
    public function getChildrenQueryBuilderProject($project = '', $node = null, $direct = false, $sortByField = null, $direction = 'ASC', $includeNode = false) {
        $meta = $this->getClassMetadata();
        $config = $this->listener->getConfiguration($this->_em, $meta->name);

        $qb = $this->getQueryBuilder();
        $qb->select('node')
                ->from($config['useObjectClass'], 'node')
                ->where($qb->expr()->eq('node.project', $project))
        ;
        if ($node !== null) {
            if ($node instanceof $meta->name) {
                $wrapped = new EntityWrapper($node, $this->_em);
                if (!$wrapped->hasValidIdentifier()) {
                    throw new InvalidArgumentException("Node is not managed by UnitOfWork");
                }
                if ($direct) {
                    $id = $wrapped->getIdentifier();
                    $qb->where($id === null ?
                                    $qb->expr()->isNull('node.' . $config['parent']) :
                                    $qb->expr()->eq('node.' . $config['parent'], is_string($id) ? $qb->expr()->literal($id) : $id)
                    );
                } else {
                    $left = $wrapped->getPropertyValue($config['left']);
                    $right = $wrapped->getPropertyValue($config['right']);
                    if ($left && $right) {
                        $qb
                                ->where($qb->expr()->lt('node.' . $config['right'], $right))
                                ->andWhere($qb->expr()->gt('node.' . $config['left'], $left))
                        ;
                    }
                }
                if (isset($config['root'])) {
                    $rootId = $wrapped->getPropertyValue($config['root']);
                    $qb->andWhere($rootId === null ?
                                    $qb->expr()->isNull('node.' . $config['root']) :
                                    $qb->expr()->eq('node.' . $config['root'], is_string($rootId) ? $qb->expr()->literal($rootId) : $rootId)
                    );
                }
                if ($includeNode) {
                    $idField = $meta->getSingleIdentifierFieldName();
                    $qb->where('(' . $qb->getDqlPart('where') . ') OR node.' . $idField . ' = :rootNode');
                    $qb->setParameter('rootNode', $node);
                }
            } else {
                throw new \InvalidArgumentException("Node is not related to this repository");
            }
        } else {
            if ($direct) {
                $qb->where($qb->expr()->isNull('node.' . $config['parent']));
            }
        }
        if (!$sortByField) {
            $qb->orderBy('node.' . $config['left'], 'ASC');
        } elseif (is_array($sortByField)) {
            $fields = '';
            foreach ($sortByField as $field) {
                $fields .= 'node.' . $field . ',';
            }
            $fields = rtrim($fields, ',');
            $qb->orderBy($fields, $direction);
        } else {
            if ($meta->hasField($sortByField) && in_array(strtolower($direction), array('asc', 'desc'))) {
                $qb->orderBy('node.' . $sortByField, $direction);
            } else {
                throw new InvalidArgumentException("Invalid sort options specified: field - {$sortByField}, direction - {$direction}");
            }
        }

        return $qb;
    }

}
